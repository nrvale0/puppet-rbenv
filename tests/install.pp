$user = 'rbenvtest'
$group = 'rbenvtest'
$user_home = '/tmp/rbenvtest'

user { $user:
  ensure => present,
  password => '!!',
  gid => $group,
  home => $user_home,
  managehome => true,
}

group { $group: ensure => present, }

rbenv::install { "${user} rbenv":
  user => $user,
  user_home => $user_home,
  group => $group,
  rbenv_ref => 'master',
  rbenv_ruby_build_ref => 'master',
  ruby_version => '2.0.0-p353',
  global_ruby_version => '2.0.0-p353',
  gems => [ 'active_yaml:0.0.1', 'cyaml:0.0.45'],
  require => User[$user],
}
