class rbenv inherits ::rbenv::params {
  file { '/etc/profile.d/rbenv.sh':
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => '[ -e ~/.rbenvrc ] && . ~/.rbenvrc'
  }

  require ::git

  ensure_packages($required_packages)
}
