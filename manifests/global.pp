define rbenv::global(
  $user,
  $user_home,
  $version
) {
  validate_string($user, $user_home, $version)

  require ::rbenv

  exec { "${user} rbenv global ruby version":
    command => "${user_home}/.rbenv/bin/rbenv global ${version} > /tmp/rbenv-${user}-global.log 2>&1",
    provider => shell,
    user => $user,
    cwd => $user_home,
    environment => "HOME=${user_home}",
    logoutput => true,
    unless => "${user_home}/.rbenv/bin/rbenv version | grep ${version}",
  }
}
