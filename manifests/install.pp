define rbenv::install(
  $user,
  $group,
  $user_home,
  $rbenv_ref,
  $ruby_version,
  $global_ruby_version,
  $rbenv_ruby_build_ref,
  $rbenv_url = '',
  $rbenv_ruby_build_url = '',
  $gems = []
) {

  require ::rbenv

  $real_rbenv_url = $rbenv_url ? { undef => $::rbenv::params::rbenv_url, default => $rbenv_url, }
  $real_rbenv_ruby_build_url = $rbenv_ruby_build_url ? { 
    undef => $::rbenv::params::rbenv_ruby_build_url, 
    default => $rbenv_ruby_build_url,
  }

  validate_string($user, $group, $rbenv_ref)
  validate_string($ruby_version)
  validate_string($global_ruby_version, $rbenv_ruby_build_ref)
  validate_absolute_path($user_home)
  validate_string($real_rbenv_url, $real_rbenv_ruby_build_url)
  validate_array($gems)

  vcsrepo { "${user} rbenv git clone":
    ensure => present,
    provider => 'git',
    user => $user,
    path => "${user_home}/.rbenv",
    source => $real_rbenv_url,
    revision => $rbenv_ref,
  } -> 

  file { "${user} rbenv plugin dir":
    ensure => directory,
    path => "${user_home}/.rbenv/plugins",
    owner => $user,
    group => $group,
  } ->

  vcsrepo { "${user} rbenv plugin ruby_build":
    ensure => present,
    provider => 'git',
    user => $user,
    path => "${user_home}/.rbenv/plugins/ruby_build",
    source => $real_rbenv_ruby_build_url,
    revision => $rbenv_ruby_build_ref,
  } ->

  file { "${user} .rbenvrc":
    ensure => file,
    path => "${user_home}/.rbenvrc",
    owner => $user,
    group => $group,
    mode => '0600',
    content => template("${module_name}/dot-rbenvrc.erb"),
  }

  file { "${user} .gemrc":
    ensure => file,
    path => "${user_home}/.gemrc",
    owner => $user,
    group => $user,
    mode => '0600',
    content => "gem: --no-document",
  }

#  $rbenv_ruby_defaults = {
#    user_home => $user_home,
#    gems => $gems,
#    require => [
#      Vcsrepo["${user} rbenv plugin ruby_build"], 
#      File["${user} .rbenvrc"],
#    ],
#  }
#
#  $ruby_versions_hash = user_ruby_versions($user, $ruby_version)
#  create_resources('::rbenv::ruby', $ruby_versions_hash, $rbenv_ruby_defaults)

  ::rbenv::ruby { $ruby_version:
    user => $user,
    user_home => $user_home,
    version => $ruby_version,
    gems => $gems,
    require => [
      Vcsrepo["${user} rbenv plugin ruby_build"],
      File["${user} .rbenvrc", "${user} .gemrc"],
    ],

  }

  rbenv::global { "default rbenv ruby for ${user}":
    user => $user,
    user_home => $user_home,
    version => $global_ruby_version,
  }

  Rbenv::Ruby <||> { before +> Rbenv::Global["default rbenv ruby for ${user}"], }
}
