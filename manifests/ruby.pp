define rbenv::ruby (
  $user,
  $user_home,
  $version,
  $gems = []
) {
 
  validate_string($user, $version)
  validate_absolute_path($user_home)
  validate_array($gems) 

  require ::rbenv
  include ::rbenv::rehash

  exec { "${user} rbenv install ${version}":
    command => "${user_home}/.rbenv/bin/rbenv install ${version} > /tmp/rbenv-ruby-${user}-${version}.log 2>&1",
    provider => shell,
    user => $user,
    cwd => $user_home,
    environment => "HOME=${user_home}",
    creates => "${user_home}/.rbenv/versions/${version}/bin/ruby",
    logoutput => true,
    timeout => 0,
  }

  $unique_gem_name = prefix($gems, "${user} - ${version} - ")
  ::rbenv::ruby::gem { $gems:
    user => $user,
    user_home => $user_home,
    ruby_version => $version,
     require => Exec["${user} rbenv install ${version}"],
  }
}
