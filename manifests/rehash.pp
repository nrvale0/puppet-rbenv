class rbenv::rehash inherits rbenv::params {
  exec { "${user} rbenv rehash":
    command => "${user_home}/.rbenv/bin/rbenv rehash",
    provider => shell,
    user => $user,
    cwd => $user_home,
    environment => "HOME=${user_home}",
    creates => "${user_home}/.rbenv/versions/${version}/bin/ruby",
    logoutput => true,
    refreshonly => true,
  }
}
