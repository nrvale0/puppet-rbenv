class rbenv::params {
  case $::osfamily {
    'redhat': {
      $required_packages = [
        'openssl-devel'
      ]
    }
    default: {fail("OS family ${::osfamily} not supported by this class!")}
  }

  $rbenv_url = 'git://github.com/sstephenson/rbenv.git'
  $rbenv_ruby_build_url = 'git://github.com/sstephenson/ruby-build.git'
}
