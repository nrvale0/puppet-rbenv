define rbenv::ruby::gem (
  $user,
  $user_home,
  $ruby_version,
) {

  validate_string($user, $ruby_version)
  validate_absolute_path($user_home)

  require ::rbenv
  include ::rbenv::rehash

  $gemandversion = split($name, ':')
  $gem = $gemandversion[0]
  $version = $gemandversion[1]

  exec { "${user} - ${ruby_version} - gem install ${name}":
    path => ['/bin','/sbin','/usr/bin','/usr/sbin'],
    provider => shell,
    user => $user,
    cwd => $user_home,
    environment => "HOME=${user_home}",
    timeout => 0,
    logoutput => true,
    unless => "source ~/.rbenvrc && rbenv shell ${ruby_version} && gem list --local -v ${version} | grep -i ${gem} | grep -i ${version}",
    command => "source ~/.rbenvrc && rbenv shell ${ruby_version} && gem install ${name} --verbose > /tmp/${user}-${ruby_version}-${gem}:${version}.log 2>&1",
#    notify => Exec["${user} rbenv rehash"],
  }
}
