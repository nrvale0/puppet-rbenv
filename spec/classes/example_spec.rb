require 'spec_helper'

describe 'rbenv' do
  context 'supported operating systems' do
    ['Debian', 'RedHat'].each do |osfamily|
      describe "rbenv class without any parameters on #{osfamily}" do
        let(:params) {{ }}
        let(:facts) {{
          :osfamily => osfamily,
        }}

        it { should compile.with_all_deps }

        it { should contain_class('rbenv::params') }
        it { should contain_class('rbenv::install').that_comes_before('rbenv::config') }
        it { should contain_class('rbenv::config') }
        it { should contain_class('rbenv::service').that_subscribes_to('rbenv::config') }

        it { should contain_service('rbenv') }
        it { should contain_package('rbenv').with_ensure('present') }
      end
    end
  end

  context 'unsupported operating system' do
    describe 'rbenv class without any parameters on Solaris/Nexenta' do
      let(:facts) {{
        :osfamily        => 'Solaris',
        :operatingsystem => 'Nexenta',
      }}

      it { expect { should contain_package('rbenv') }.to raise_error(Puppet::Error, /Nexenta not supported/) }
    end
  end
end
