module Puppet::Parser::Functions
  newfunction(:user_ruby_versions, :type => :rvalue) do |args|

    unless 2 == args.length then
      raise Puppet::ParseError, "user_ruby_verions: wrong number of parameters"
    end

    user, versions = args
    versions_hash = {}

    versions.each do |version|
      versions_hash["#{user} - #{version}"] = { 'version' => version , 'user' => user }
    end
    return versions_hash

  end
end
